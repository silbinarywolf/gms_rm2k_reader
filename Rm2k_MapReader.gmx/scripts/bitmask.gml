var a = argument0;
var b = argument1;

var r = 0;
for (var i=a; i<=b; i++)
{
    r |= 1 << i;
}

return r;
