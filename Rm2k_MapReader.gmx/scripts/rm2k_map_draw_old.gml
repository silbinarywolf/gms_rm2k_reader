/*--------------------------------------------------------------------------------------
*   rm2k_map_draw
*
*   Renders the map data provided by rm2k_read_lmu
*
*   arguemnt0 - Map Data (Optional)
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var BLOCK_C = 3000;

var BLOCK_D = 4000;
var BLOCK_D_BLOCKS = 12;

var BLOCK_E = 5000;
var BLOCK_E_TILES = 144;

var BLOCK_F = 10000;
var BLOCK_F_TILES = 144;

var map_data;
if (argument_count == 1)
{
    map_data = argument[0];
}
else
{
    map_data = _rm2k_map;
}

var map_width = ds_map_find_value(map_data, "width");
var map_height = ds_map_find_value(map_data, "height");
var map_lowerTiles = ds_map_find_value(map_data, "lowerTiles");

// Setup animation frame variables
var map_animation_offset_ab, map_animation_offset_c;
if (_rm2k_anim_frame == _rm2k_anim_frame_speed)
{
    map_animation_offset_ab = 1;
    map_animation_offset_c = 1;
}
else if (_rm2k_anim_frame == _rm2k_anim_frame_speed * 2)
{
    map_animation_offset_ab = 2;
    map_animation_offset_c = 2;
}
else if (_rm2k_anim_frame == _rm2k_anim_frame_speed * 3)
{
    if (_rm2k_anim_type == 1)
    {
        // If animation type is 1-2-3-2
        map_animation_offset_ab = 1;
    }
    else
    {
        // If animation type is 1-2-3
        map_animation_offset_ab = 0;
    }
    map_animation_offset_c = 3;
}
else
{
    map_animation_offset_ab = 0;
    map_animation_offset_c = 0;
}
map_animation_offset_ab *= 16;
map_animation_offset_c *= 16;

// Calculate where on the screen to start drawing
var map_draw_x_default = view_xview[view_current] - 16;
var map_draw_y_default = view_yview[view_current] - 16;
var map_draw_x = map_draw_x_default;
var map_draw_y = map_draw_y_default;

// Calculate what region of tiles to draw based on the view
var tiles_x_start = map_draw_x_default / 16
var tiles_y_start = map_draw_y_default / 16;
var tiles_x_end = view_xview[view_current] + view_wview[view_current] + 16;
var tiles_y_end = view_yview[view_current] + view_hview[view_current] + 16;
tiles_x_end = (tiles_x_end / 16);
tiles_y_end = (tiles_y_end / 16);
if (tiles_x_start <= 0)
{
    tiles_x_start = 0;
}
if (tiles_y_start <= 0)
{
    tiles_y_start = 0;
}
if (tiles_x_end > map_width)
{
    tiles_x_end = map_width;
}
if (tiles_y_end > map_height)
{
    tiles_y_end = map_height;
}
var map_tile_info, map_tile_subinfo;

//if (!surface_exists(global._rm2k_surface))
{
    //global._rm2k_surface = surface_create(1024,1024);
    //surface_set_target(global._rm2k_surface);
    for (var yy = tiles_y_start; yy < tiles_y_end; ++yy)
    {
        for (var xx = tiles_x_start; xx < tiles_x_end; ++xx)
        {
            map_tile_info = map_lowerTiles[yy, xx];
            if (map_tile_info[0] < BLOCK_C) // BLOCK AB
            {
                for (i = 0; i < 4; ++i)
                {
                    map_tile_subinfo = map_tile_info[2+i];
                    if (is_array(map_tile_subinfo))
                    {
                        draw_background_part(bgBasis, map_tile_subinfo[0] + map_animation_offset_ab, map_tile_subinfo[1], 8, 8, map_draw_x + map_tile_subinfo[2], map_draw_y + map_tile_subinfo[3]);
                    }
                }
            }
            else if (map_tile_info[0] == BLOCK_D)
            {
                for (i = 0; i < 4; ++i)
                {
                    map_tile_subinfo = map_tile_info[2+i];
                    draw_background_part(bgBasis, map_tile_subinfo[0], map_tile_subinfo[1], 8, 8, map_draw_x + map_tile_subinfo[2], map_draw_y + map_tile_subinfo[3]);
                }
            }
            else if (map_tile_info[0] >= BLOCK_C && map_tile_info[0] < BLOCK_D)
            {
                draw_background_part(bgBasis, map_tile_info[2], map_tile_info[3] + map_animation_offset_c, 16, 16, map_draw_x, map_draw_y);
            }
            else
            {
                draw_background_part(bgBasis, map_tile_info[2], map_tile_info[3], 16, 16, map_draw_x, map_draw_y);
            }
            map_draw_x += 16;
        }
        map_draw_x = map_draw_x_default;
        map_draw_y += 16;
    }
    //surface_reset_target();
}
//draw_surface(global._rm2k_surface, 0, 0);

