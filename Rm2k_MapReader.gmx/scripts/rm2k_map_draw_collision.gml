/*--------------------------------------------------------------------------------------
*   rm2k_map_draw_collision
*
*   Renders the map collsion boxes, this is used for debugging mainly
*
*   argument0 - Map Data (Optional)
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/

var map_data;
if (argument_count == 1)
{
    map_data = argument[0];
}
else
{
    map_data = _rm2k_map;
}

var map_width = ds_map_find_value(map_data, "width");
var map_height = ds_map_find_value(map_data, "height");

// Calculate where on the screen to start drawing
// or use the provided destination coords.
var map_draw_x_default = view_xview[view_current];
var map_draw_y_default = view_yview[view_current];
var map_draw_x, map_draw_y;
map_draw_x_default = floor(map_draw_x_default / 16) * 16;
map_draw_y_default = floor(map_draw_y_default / 16) * 16;
map_draw_x = map_draw_x_default;
map_draw_y = map_draw_y_default;

// Calculate what region of tiles to draw based on the parameters
var tiles_x_start = floor((view_xview[view_current]) / 16);
var tiles_y_start = floor((view_yview[view_current]) / 16);
var tiles_x_end = view_xview[view_current] + view_wview[view_current];
var tiles_y_end = view_yview[view_current] + view_hview[view_current];
tiles_x_end = ceil(tiles_x_end / 16);
tiles_y_end = ceil(tiles_y_end / 16);
if (tiles_x_start <= 0)
{
    tiles_x_start = 0;
}
if (tiles_y_start <= 0)
{
    tiles_y_start = 0;
}
if (tiles_x_end > map_width)
{
    tiles_x_end = map_width;
}
if (tiles_y_end > map_height)
{
    tiles_y_end = map_height;
}

var map_collision = ds_map_find_value(map_data, "collision");

// Draw Collision Boxes
draw_set_color(c_red);
draw_set_alpha(0.7);
for (var yy = tiles_y_start; yy < tiles_y_end; ++yy)
{
    for (var xx = tiles_x_start; xx < tiles_x_end; ++xx)
    {
        if (ds_grid_get(map_collision, xx, yy) > 0)
        {
            draw_rectangle(map_draw_x,map_draw_y,map_draw_x+15,map_draw_y+15,false);
        }
        map_draw_x += 16;
    }
    map_draw_x = map_draw_x_default;
    map_draw_y += 16;
}
draw_set_alpha(1);
draw_set_color(c_black);
