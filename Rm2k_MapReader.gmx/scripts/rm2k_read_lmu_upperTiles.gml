/*--------------------------------------------------------------------------------------
*   rm2k_read_lmu_upperTiles
*
*   Reads the tiles, puts it into a quick array for rendering
*
*   argument0 - Buffer
*   argument1 - Map Data
*   argument2 - Database
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var BLOCK_F = 10000;
var BLOCK_F_TILES = 144;

var buffer = argument0;
var map_data = argument1;
var database = argument2;

var map_chipset_id = ds_map_find_value(map_data, "id");
var map_width = ds_map_find_value(map_data, "width");
var map_height = ds_map_find_value(map_data, "height");
var map_collision = ds_map_find_value(map_data, "collision");

var database_passable;
if (database != -1)
{
    var database_chipsets = ds_map_find_value(database, "chipset");
    var database_chipset = ds_list_find_value(database_chipsets, map_chipset_id);
    database_passable = ds_map_find_value(database_chipset, "upperTiles");
}
else
{
    database_passable = -1;
}

var map_tile_info = 0;
var map_tile = -1;
var map_tile_data_size = buffer_read_ber(buffer);

// Init Tile Data Array(s)
var map_tiles = 0;

// Read tiles from LMU
map_tiles[map_height-1,map_width-1] = 0;
for (var yy = 0; yy < map_height; ++yy)
{
    for (var xx = 0; xx < map_width; ++xx)
    {
        // Get the tile ID
        map_tile = buffer_read(buffer, buffer_u16);
        if (map_tile > BLOCK_F && map_tile < BLOCK_F + BLOCK_F_TILES) {
            var map_tile_id = map_tile - BLOCK_F;
            var row, col;
            
            // Get the tile coordinates from chipset
            if (map_tile_id < 48) {
                // If from first column of the block
                col = floor(18 + map_tile_id % 6);
                row = 8 + floor(map_tile_id / 6);
            } else {
                // If from second column of the block
                col = 24 + (map_tile_id - 48) % 6;
                row = floor((map_tile_id - 48) / 6);
            }
            map_tile_info[2] = (row << 4); // Fast Multiply by 16
            map_tile_info[1] = (col << 4);
            map_tile_info[0] = map_tile;
             
            // If Database loaded, get collision data
            if (is_array(database_passable))
            {
                if (ds_grid_get(map_collision,xx,yy) == 0)
                {
                    ds_grid_set(map_collision,xx,yy,database_passable[map_tile_id]);
                }
            }
        }
        map_tiles[yy,xx] = map_tile_info;
        map_tile_info = BLOCK_F;
    }
}
ds_map_add(map_data, "upperTiles", map_tiles);
return map_tiles;
