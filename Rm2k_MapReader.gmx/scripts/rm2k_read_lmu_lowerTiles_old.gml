/*--------------------------------------------------------------------------------------
*   rm2k_read_lmu_lowerTiles
*
*   Reads the tiles and puts it into the map
*
*   argument0 - Buffer
*   arguemnt1 - Map Data
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var TILES_PER_ROW = 64;
var BLOCK_C = 3000;

var BLOCK_D = 4000;
var BLOCK_D_BLOCKS = 12;

var BLOCK_E = 5000;
var BLOCK_E_TILES = 144;

var BLOCK_F = 10000;
var BLOCK_F_TILES = 144;

var buffer = argument0;
var map_data = argument1;

var map_width = ds_map_find_value(map_data, "width");
var map_height = ds_map_find_value(map_data, "height");

var map_tile = -1;
var map_tile_data_size = buffer_read_ber(buffer, buffer_u16);

// Init Tile Data Array(s)
var map_tiles = 0;

// Read tiles from LMU
map_tiles[map_height,map_width] = 0;
for (var yy = 0; yy < map_height; ++yy)
{
    for (var xx = 0; xx < map_width; ++xx)
    {
        // Get the tile ID
        map_tile = buffer_read(buffer, buffer_u16);
        map_tiles[yy,xx] = map_tile;
        
        if (map_tile < BLOCK_C) {
            // Generate Autotile AB
            rm2k_read_lmu_genAB(map_autotiles_ab, map_tile, 0);
            rm2k_read_lmu_genAB(map_autotiles_ab, map_tile, 1);
            rm2k_read_lmu_genAB(map_autotiles_ab, map_tile, 2);
        }
        else
        {
            // Generate Autotile D
        }
    }
   // game_end();
   // return false;
}
ds_map_add(map_data, "lowerTiles", map_tiles);
return map_tiles;
