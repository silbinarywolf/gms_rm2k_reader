/*--------------------------------------------------------------------------------------
*   rm2k_init
*
*   Initiates RPG Maker 2000 support, mainly by creating the BlockAB/BlockD lookup
*   tables.
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
global._rm2k_blockA_subtiles = rm2k_init_blockA();
global._rm2k_blockD_subtiles = rm2k_init_blockD();
global._rm2k_database = -1;
global._rm2k_collision = -1;
global._rm2k_errors = "";
return true;
