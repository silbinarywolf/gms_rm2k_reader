var time_diff_msg = "Time difference: " + string(current_time - global._benchmark) + "ms";
show_debug_message(time_diff_msg);
if (argument_count >= 1)
{
    if (argument[0])
    {
        show_message(time_diff_msg);
    }
}
