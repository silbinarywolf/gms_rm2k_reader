/// buffer_read_char(buffer, length)
/*--------------------------------------------------------------------------------------
*   buffer_read_char
*
*   Reads a string based on the length provided
*
*   argument0 - Buffer
*   argument1 - String Length
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var value = "";
var buffer = argument0;
var length = argument1;
for (var i = 0; i < length; ++i)
{
    value += chr(buffer_read(buffer, buffer_u8));
}
return value;
