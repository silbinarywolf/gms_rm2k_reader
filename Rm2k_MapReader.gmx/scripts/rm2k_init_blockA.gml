/*--------------------------------------------------------------------------------------
*   rm2k_init_blockA
*
*   Creates a 3D array for Block A of a RPG maker Tileset
*   These values help calculate the autotiles for water.
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/

// BlockA_Subtiles_IDS[47][2][2]
// [tile-id][row][col]
var N = -1;
var i = 0;
var blockA = 0;

blockA[47] = 0;
blockA[i] = rm2k_init_blockA_item(N,N,N,N); i++;
blockA[i] = rm2k_init_blockA_item(3,N,N,N); i++;
blockA[i] = rm2k_init_blockA_item(N,3,N,N); i++;
blockA[i] = rm2k_init_blockA_item(3,3,N,N); i++;
blockA[i] = rm2k_init_blockA_item(N,N,N,3); i++;
blockA[i] = rm2k_init_blockA_item(3,N,N,3); i++;
blockA[i] = rm2k_init_blockA_item(N,3,N,3); i++;
blockA[i] = rm2k_init_blockA_item(3,3,N,3); i++;
blockA[i] = rm2k_init_blockA_item(N,N,3,N); i++;
blockA[i] = rm2k_init_blockA_item(3,N,3,N); i++;
blockA[i] = rm2k_init_blockA_item(N,3,3,N); i++;
blockA[i] = rm2k_init_blockA_item(3,3,3,N); i++;
blockA[i] = rm2k_init_blockA_item(N,N,3,3); i++;
blockA[i] = rm2k_init_blockA_item(3,N,3,3); i++;
blockA[i] = rm2k_init_blockA_item(N,3,3,3); i++;
blockA[i] = rm2k_init_blockA_item(3,3,3,3); i++;
blockA[i] = rm2k_init_blockA_item(1,N,1,N); i++;
blockA[i] = rm2k_init_blockA_item(1,3,1,N); i++;
blockA[i] = rm2k_init_blockA_item(1,N,1,3); i++;
blockA[i] = rm2k_init_blockA_item(1,3,1,3); i++;
blockA[i] = rm2k_init_blockA_item(2,2,N,N); i++;
blockA[i] = rm2k_init_blockA_item(2,2,N,3); i++;
blockA[i] = rm2k_init_blockA_item(2,2,3,N); i++;
blockA[i] = rm2k_init_blockA_item(2,2,3,3); i++;
blockA[i] = rm2k_init_blockA_item(N,1,N,1); i++;
blockA[i] = rm2k_init_blockA_item(N,1,3,1); i++;
blockA[i] = rm2k_init_blockA_item(3,1,N,1); i++;
blockA[i] = rm2k_init_blockA_item(3,1,3,1); i++;
blockA[i] = rm2k_init_blockA_item(N,N,2,2); i++;
blockA[i] = rm2k_init_blockA_item(3,N,2,2); i++;
blockA[i] = rm2k_init_blockA_item(N,3,2,2); i++;
blockA[i] = rm2k_init_blockA_item(3,3,2,2); i++;
blockA[i] = rm2k_init_blockA_item(1,1,1,1); i++;
blockA[i] = rm2k_init_blockA_item(2,2,2,2); i++;
blockA[i] = rm2k_init_blockA_item(0,2,1,N); i++;
blockA[i] = rm2k_init_blockA_item(0,2,1,3); i++;
blockA[i] = rm2k_init_blockA_item(2,0,N,1); i++;
blockA[i] = rm2k_init_blockA_item(2,0,3,1); i++;
blockA[i] = rm2k_init_blockA_item(N,1,2,0); i++;
blockA[i] = rm2k_init_blockA_item(3,1,2,0); i++;
blockA[i] = rm2k_init_blockA_item(1,N,0,2); i++;
blockA[i] = rm2k_init_blockA_item(1,3,0,2); i++;
blockA[i] = rm2k_init_blockA_item(0,0,1,1); i++;
blockA[i] = rm2k_init_blockA_item(0,2,0,2); i++;
blockA[i] = rm2k_init_blockA_item(1,1,0,0); i++;
blockA[i] = rm2k_init_blockA_item(2,0,2,0); i++;
blockA[i] = rm2k_init_blockA_item(0,0,0,0); i++;
return blockA;
