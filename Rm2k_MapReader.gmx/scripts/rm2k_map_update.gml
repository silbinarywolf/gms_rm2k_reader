/*--------------------------------------------------------------------------------------
*   rm2k_map_update
*
*   Updates the animation of the map tiles.
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
if (_rm2k_layer == 0)
{
    _rm2k_current_frame += 1;
    if (_rm2k_current_frame >= room_speed >> 2) // Approx every 250ms (1/4 of a sec)
    {
        _rm2k_anim_frame += _rm2k_anim_frame_speed;
        if (_rm2k_anim_frame >= _rm2k_anim_frame_speed * 4)
        {
            _rm2k_anim_frame = 0;
        }
        _rm2k_current_frame = 0;
    }
}
