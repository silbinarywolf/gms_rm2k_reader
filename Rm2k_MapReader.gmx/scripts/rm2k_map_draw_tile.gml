/*--------------------------------------------------------------------------------------
*   rm2k_map_draw_tile
*
*   Draws the provided map.
*
*   argument0 - X
*   argument1 - Y
*   argument2 - Tile Src Row
*   argument3 - Tile Src Col
*
*   @author SilbinaryWolf, Ghabry (https://github.com/Ghabry)
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
draw_background_part(bgBasis, argument[3] * 16, argument[2] * 16, 16, 16,argument[0], argument[1]);

