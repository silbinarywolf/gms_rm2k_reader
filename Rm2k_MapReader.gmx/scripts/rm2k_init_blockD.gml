/*--------------------------------------------------------------------------------------
*   rm2k_init_blockD
*
*   Creates a 4D array for Block D of a RPG maker Tileset
*   These values help calculate the autotiles for grass, dirt, etc.
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/

var i = 0;
var blockD = 0;

blockD[50] = 0;
blockD[i] = rm2k_init_blockD_item(1,2,1,2 , 1,2,1,2); i++;
blockD[i] = rm2k_init_blockD_item(2,0,1,2 , 1,2,1,2); i++;
blockD[i] = rm2k_init_blockD_item(1,2,2,0 , 1,2,1,2); i++;
blockD[i] = rm2k_init_blockD_item(2,0,2,0 , 1,2,1,2); i++;

blockD[i] = rm2k_init_blockD_item(1,2,1,2 , 1,2,2,0); i++;
blockD[i] = rm2k_init_blockD_item(2,0,1,2 , 1,2,2,0); i++;
blockD[i] = rm2k_init_blockD_item(1,2,2,0 , 1,2,2,0); i++;
blockD[i] = rm2k_init_blockD_item(2,0,2,0 , 1,2,2,0); i++;

blockD[i] = rm2k_init_blockD_item(1,2,1,2 , 2,0,1,2); i++;
blockD[i] = rm2k_init_blockD_item(2,0,1,2 , 2,0,1,2); i++;
blockD[i] = rm2k_init_blockD_item(1,2,2,0 , 2,0,1,2); i++;
blockD[i] = rm2k_init_blockD_item(2,0,2,0 , 2,0,1,2); i++;

blockD[i] = rm2k_init_blockD_item(1,2,1,2 , 2,0,2,0); i++;
blockD[i] = rm2k_init_blockD_item(2,0,1,2 , 2,0,2,0); i++;
blockD[i] = rm2k_init_blockD_item(1,2,2,0 , 2,0,2,0); i++;
blockD[i] = rm2k_init_blockD_item(2,0,2,0 , 2,0,2,0); i++;

blockD[i] = rm2k_init_blockD_item(0,2,0,2 , 0,2,0,2); i++;
blockD[i] = rm2k_init_blockD_item(0,2,2,0 , 0,2,0,2); i++;
blockD[i] = rm2k_init_blockD_item(0,2,0,2 , 0,2,2,0); i++;
blockD[i] = rm2k_init_blockD_item(0,2,2,0 , 0,2,2,0); i++;

blockD[i] = rm2k_init_blockD_item(1,1,1,1 , 1,1,1,1); i++;
blockD[i] = rm2k_init_blockD_item(1,1,1,1 , 1,1,2,0); i++;
blockD[i] = rm2k_init_blockD_item(1,1,1,1 , 2,0,1,1); i++;
blockD[i] = rm2k_init_blockD_item(1,1,1,1 , 2,0,2,0); i++;

blockD[i] = rm2k_init_blockD_item(2,2,2,2 , 2,2,2,2); i++;
blockD[i] = rm2k_init_blockD_item(2,2,2,2 , 2,0,2,2); i++;
blockD[i] = rm2k_init_blockD_item(2,0,2,2 , 2,2,2,2); i++;
blockD[i] = rm2k_init_blockD_item(2,0,2,2 , 2,0,2,2); i++;

blockD[i] = rm2k_init_blockD_item(1,3,1,3 , 1,3,1,3); i++;
blockD[i] = rm2k_init_blockD_item(2,0,1,3 , 1,3,1,3); i++;
blockD[i] = rm2k_init_blockD_item(1,3,2,0 , 1,3,1,3); i++;
blockD[i] = rm2k_init_blockD_item(2,0,2,0 , 1,3,1,3); i++;

blockD[i] = rm2k_init_blockD_item(0,2,2,2 , 0,2,2,2); i++;
blockD[i] = rm2k_init_blockD_item(1,1,1,1 , 1,3,1,3); i++;
blockD[i] = rm2k_init_blockD_item(0,1,0,1 , 0,1,0,1); i++;
blockD[i] = rm2k_init_blockD_item(0,1,0,1 , 0,1,2,0); i++;

blockD[i] = rm2k_init_blockD_item(2,1,2,1 , 2,1,2,1); i++;
blockD[i] = rm2k_init_blockD_item(2,1,2,1 , 2,0,2,1); i++;
blockD[i] = rm2k_init_blockD_item(2,3,2,3 , 2,3,2,3); i++;
blockD[i] = rm2k_init_blockD_item(2,0,2,3 , 2,3,2,3); i++;

blockD[i] = rm2k_init_blockD_item(0,3,0,3 , 0,3,0,3); i++;
blockD[i] = rm2k_init_blockD_item(0,3,2,0 , 0,3,0,3); i++;
blockD[i] = rm2k_init_blockD_item(0,1,2,1 , 0,1,2,1); i++;
blockD[i] = rm2k_init_blockD_item(0,1,0,1 , 0,3,0,3); i++;

blockD[i] = rm2k_init_blockD_item(0,3,2,3 , 0,3,2,3); i++;
blockD[i] = rm2k_init_blockD_item(2,1,2,1 , 2,3,2,3); i++;
blockD[i] = rm2k_init_blockD_item(0,1,2,1 , 0,3,2,3); i++;
blockD[i] = rm2k_init_blockD_item(1,2,1,2 , 1,2,1,2); i++;

blockD[i] = rm2k_init_blockD_item(1,2,1,2 , 1,2,1,2); i++;
blockD[i] = rm2k_init_blockD_item(0,0,0,0 , 0,0,0,0); i++;
return blockD;
