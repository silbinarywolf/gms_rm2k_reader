/*--------------------------------------------------------------------------------------
*   rm2k_map_free
*
*   Frees any surfaces or misc data created by the map object.
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/

