/// rm2k_read_ldb_chipset(buffer, database)
/*--------------------------------------------------------------------------------------
*   rm2k_read_ldb_chipset
*
*   Reads the Chipset data from the Rm2k Database
*
*   argument0 - Buffer
*
*   @author SilbinaryWolf
*   @source http://rpg2kdev.sue445.net/?RPG2000%2F%A5%C7%A1%BC%A5%BF%A5%D9%A1%BC%A5%B9%2F%A5%C1%A5%C3%A5%D7%A5%BB%A5%C3%A5%C8
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var seq_id, seq_length, value, background_ind, value_arr, i;
var buffer = argument0;
var chipsets = ds_list_create();
var chipset_count = buffer_read_ber(buffer); // the number of chipsets in the DB

for (var chipset_ind = 0; chipset_ind < chipset_count; ++chipset_ind)
{
    var chipset = ds_map_create();
    
    buffer_read_ber(buffer);  // Chipset ID
    while (true)
    {
        seq_id = buffer_read(buffer, buffer_u8);
        if (seq_id != 0)
        {
            seq_length = buffer_read_ber(buffer);
        }
        else
        {
            break;
        }

        //show_debug_message("Chipset Seq ID: " + string(chipset_ind) + " " + string(seq_id) + " " + string(seq_length));
        switch (seq_id)
        {
            // Chipset Name
            case 1: 
                value = buffer_read_char(buffer, seq_length);
                ds_map_add(chipset, "name", value);
                background_ind = asset_get_index(value);
                if (background_ind == -1)
                {
                    global._rm2k_errors += "Rm2k Database - Couldn't find '" + string(value) + "' as a background.#Make sure to make it the chipset name identical to the Game Maker background index it's relevant to.##";
                }
                else
                { 
                    ds_map_add(chipset, "background_index", background_ind);
                }
                //show_message("Chipset Name: " + string(value));
            break;
            
            // Chipset Filename
            /*case 2:
                buffer_seek(buffer, buffer_seek_relative, seq_length);
            break;*/
            
            // Terrain IDs of Lower Tiles
            // Format: uint16_t [162]
            /*case 3:
                for (i = 0; i < 162; ++i)
                {
                    value = buffer_read(buffer, buffer_u16);
                }
            break;*/
            
            // Format/Traffic Block of Lower Tiles
            // Format: uint8_t [162]
            case 4:
                value_arr[seq_length] = 0;
                for (i = 0; i < seq_length; ++i)
                {
                    value = buffer_read(buffer, buffer_u8);
                    var passable = value & 31; // 0x1F - Read first 4 bits, passable directions (down, left, right, up)
                    value_arr[i] = (passable != 15);
                }
                ds_map_add(chipset, "lowerTiles", value_arr);
                value_arr = 0;
            break;
        
            // Format/Traffic Block of Upper Tiles
            // Format: uint8_t [144]
            case 5:
                value_arr[seq_length] = 0;
                for (i = 0; i < seq_length; ++i)
                {
                    value = buffer_read(buffer, buffer_u8);
                    var passable = value & 31; // 0x1F - Read first 4 bits, passable directions (down, left, right, up)
                    value_arr[i] = passable;
                }
                ds_map_add(chipset, "upperTiles", value_arr);
                value_arr = 0;
            break;
            
            // Sea Animation Type
            //  0: 1-2-3-2
            //  1: 1-2-3
            case 11:
                value = buffer_read(buffer, buffer_u8);
                ds_map_add(chipset, "seaAnimationType", value);
            break;
            
            // Sea Animate Speed
            //  0: Low-speed 
            //  1: High-speed
            case 12:
                value = buffer_read(buffer, buffer_u8);
                ds_map_add(chipset, "seaAnimationSpeed", value);
            break;
            
            default:
                // Skip unsupported/unimplemented sections
                buffer_seek(buffer, buffer_seek_relative, seq_length);
            break;
        }
    }
    
    ds_list_add(chipsets, chipset);
}
return chipsets;
