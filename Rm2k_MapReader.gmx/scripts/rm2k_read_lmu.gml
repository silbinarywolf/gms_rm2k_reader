/*--------------------------------------------------------------------------------------
*   rm2k_read_lmu
*
*   Reads the map data
*
*   argument0 - Filename
*   argument1 - Database (optional, otherwise uses default)
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var xx = 0;
var yy = 0;

var map_width, map_height;
map_width = 20;
map_height = 15;

// Setup RPG Maker Map defaults
// Some values aren't written into the map file
// and so they default to specific values.
var map_data = ds_map_create();
ds_map_add(map_data, "id", 1);
ds_map_add(map_data, "background_index", -1);
ds_map_add(map_data, "width", map_width);
ds_map_add(map_data, "height", map_height);

// Load the RPG Maker database to get collision support
// and background matching (Chipset Name must match Background Asset Name)
var database;
if (argument_count > 1)
{
    database = argument[1];
}
else
{
    database = global._rm2k_database;
}

var map_tile_data_size = 0;
var map_tile;
var quit = false;

var buffer = buffer_load(argument[0]);
buffer_seek(buffer, buffer_seek_start, 11);
while (quit == false)
{
    // Read the sequence ID
    var seq_id = buffer_read(buffer, buffer_u8);
    
    // Read length of the sequence
    var seq_length, seq_value;
    seq_length = buffer_read(buffer, buffer_u8);
    switch (seq_id)
    {
        // Chipset ID
        case 1:
            seq_value = buffer_read_ber(buffer);
            ds_map_replace(map_data, "id", seq_value);
        break;
    
        // Width
        case 2:
            seq_value = buffer_read_ber(buffer);
            ds_map_replace(map_data, "width", seq_value);
            map_width = seq_value;
        break;
        
        // Height
        case 3:
            seq_value = buffer_read_ber(buffer);
            ds_map_replace(map_data, "height", seq_value);
            map_height = seq_value;
        break;
        
        // Scroll Type
        /*case 11: // 0x0B
        
        break;
        
        // Using Panorama/Landscape File(Boolean)
        case 31: // 0x1F
            
        break;
        
        // Panorama Landscape Filename
        case 32: // 0x20
            seq_value = buffer_read(buffer, buffer_string);
        break;*/
        
        // Lower Tile Map
        case 71: // 0x47
            // setup collision grid
            if (!ds_map_exists(map_data, "collision"))
            {
                ds_map_add(map_data, "collision", ds_grid_create(map_width,map_height));
            }
            rm2k_read_lmu_lowerTiles(buffer, map_data, database);
        break;
        
        // Upper Tile Map
        case 72: // 0x48
            // setup collision grid
            if (!ds_map_exists(map_data, "collision"))
            {
                ds_map_add(map_data, "collision", ds_grid_create(map_width,map_height));
            }
            rm2k_read_lmu_upperTiles(buffer, map_data, database);
        break;
        
        // Events
        case 81: // 0x51
            rm2k_read_lmu_events(buffer, map_data, database);
            quit = true;
        break;
        
        default:
            // Skip unsupported/unimplemented sections
            buffer_seek(buffer, buffer_seek_relative, seq_length);
        break;
    }
}
// If Database is available
// Read the background_index of the chipset being used
// by the map.
if (ds_exists(database, ds_type_map))
{
    var database_chipsets = ds_map_find_value(database, "chipset");
    var database_chipset = ds_list_find_value(database_chipsets, seq_value);
    seq_value = ds_map_find_value(database_chipset, "background_index");
    ds_map_replace(map_data, "background_index", seq_value);
    // Catch any game breaking errors
    if (seq_value == -1)
    {
        seq_value = ds_map_find_value(database_chipset, "name");
        show_message("RPG Maker 2000 Error: Chipset background could not be obtained during asset_get_index. Background with name '" + string(seq_value)+ "' could not be found.");
       // game_end();
    }
}
return map_data;
