/*--------------------------------------------------------------------------------------
*   rm2k_read_lmu_events
*
*   Reads the Rm2k map events
*
*   argument0 - Buffer
*   argument1 - Map Data
*   argument2 - Database
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var value, seq_id, seq_length;
var buffer = argument0;
var map_data = argument1;
var database = argument2;

var map_event_data_size = buffer_read_ber(buffer);

// If there is map event data to load, load it.
if (map_event_data_size > 0)
{
    var map_event_count = buffer_read_ber(buffer); // the number of events on the map
    var map_events = ds_list_create();
    
    for (var event_ind = 0; event_ind < map_event_count; ++event_ind)
    {
        var map_event = ds_map_create();
        
        buffer_read_ber(buffer);  // Event ID
        while (true)
        {
            seq_id = buffer_read(buffer, buffer_u8);
            if (seq_id != 0)
            {
                seq_length = buffer_read_ber(buffer);
            }
            else
            {
                break;
            }
            switch (seq_id)
            {
                // Event Name
                case 1:
                    value = buffer_read_char(buffer, seq_length);
                    ds_map_add(map_event, "name", value);
                    
                    // Get custom object index if not default event name
                    var pos, object_ind, object_ind_name;
                    pos = string_pos("_", value);
                    if (pos > 0)
                    {
                        object_ind_name = string_copy(value, pos+1, string_length(value)-pos);
                        object_ind = asset_get_index(object_ind_name); 
                    }
                    else
                    {
                        object_ind_name = value;
                        object_ind = asset_get_index(value);
                    }
                    // Log error if no object found
                    if (object_ind == -1)
                    {
                        global._rm2k_errors += "Rm2k Map Events - Couldn't find '" + string(object_ind_name) + "' as an object.#Make sure to make the object name identical to the Game Maker object index it's relevant to.##";
                    }
                    ds_map_add(map_event, "object_index", object_ind);
                break;
                
                // Event X
                case 2:
                    value = buffer_read_ber(buffer);
                    ds_map_add(map_event, "x", value);
                break;
                
                // Event Y
                case 3:
                    value = buffer_read_ber(buffer);
                    ds_map_add(map_event, "y", value);
                break;
                
                default:
                    // Skip unsupported/unimplemented sections
                    buffer_seek(buffer, buffer_seek_relative, seq_length);
                break;
            }
        }
        ds_list_add(map_events, map_event);
    }
    ds_map_add(map_data, "events", map_events);
}
else
{
    ds_map_add(map_data, "events", -1);
}
return true;
