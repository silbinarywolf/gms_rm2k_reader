/// rm2k_map_create(map_data, lowertile_object_index, uppertile_object_index)
/*--------------------------------------------------------------------------------------
*   rm2k_map_create
*
*   Creates an object setup to easily render RPG Maker maps.
*
*   argument0 - Map Data
*   argument1 - Map Renderer Lower Object Index
*   argument2 - Map Renderer Higher Object Index
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var i = instance_create(0,0,argument1);
i._rm2k_map = argument0;
i._rm2k_layer = 0; // 0 - lower, 1 - upper

// Set default collision checks to use the most recently created map
global._rm2k_collision = ds_map_find_value(argument0, "collision");

with (i)
{
    // Handle chipset animations
    _rm2k_current_frame = 0;
    _rm2k_anim_frame = 0;
    _rm2k_anim_type = 1; // TODO: Read from database
    _rm2k_anim_frame_speed = 1;
}

var upperTiles = ds_map_find_value(argument0, "upperTiles");
if (is_array(upperTiles))
{   
    i = instance_create(0,0,argument2);
    i._rm2k_map = argument0;
    i._rm2k_layer = 1; // 0 - lower, 1 - upper
}
else
{
    show_debug_message("Rm2k Map Create - No Upper Tiles");
}

var events = ds_map_find_value(argument0, "events");
if (ds_exists(events, ds_type_list))
{
    for (var event_ind = 0; event_ind < ds_list_size(events); ++event_ind)
    {
        var event_data;
        var event_data_object_index, event_data_x, event_data_y;
        event_data = ds_list_find_value(events, event_ind);
        event_data_object_index = ds_map_find_value(event_data, "object_index");
        event_data_x = ds_map_find_value(event_data, "x") << 4; // Quick Multiply by 16
        event_data_y = ds_map_find_value(event_data, "y") << 4;
        if (event_data_object_index != -1)
        {
            i = instance_create(event_data_x, event_data_y, event_data_object_index);
        }
    }
}
else
{
    show_debug_message("Rm2k Map Create - No Events");
}
