/*--------------------------------------------------------------------------------------
*   rm2k_init_blockD_item
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var item = ds_grid_create(2,2);

var subitem;
subitem[1] = argument1;
subitem[0] = argument0;
ds_grid_set(item, 0, 0, subitem);

var subitem2;
subitem2[1] = argument3; // y
subitem2[0] = argument2; // x
ds_grid_set(item, 1, 0, subitem2);

var subitem3;
subitem3[1] = argument5; // y 
subitem3[0] = argument4; // x
ds_grid_set(item, 0, 1, subitem3); 

var subitem4;
subitem4[1] = argument7;
subitem4[0] = argument6; 
ds_grid_set(item, 1, 1, subitem4);
return item;
