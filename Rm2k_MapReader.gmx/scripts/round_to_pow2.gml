// round_to_pow2(value)
/*--------------------------------------------------------------------------------------
*   round_to_pow2
*
*   Rounds the provided value up to the next power of 2
*
*   argument0 - Value
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var v = argument0;
v--;
v |= v >> 1;
v |= v >> 2;
v |= v >> 4;
v |= v >> 8;
v |= v >> 16;
v++;
return v;
