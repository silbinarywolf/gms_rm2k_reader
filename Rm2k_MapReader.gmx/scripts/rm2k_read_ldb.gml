/// rm2k_read_ldb(filename)
/*--------------------------------------------------------------------------------------
*   rm2k_read_ldb
*
*   Renders the database data provided by rm2k_read_ldb
*
*   Returns Database
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var database = ds_map_create();
var quit = false;
global._rm2k_database = database;

var buffer = buffer_load(argument0);
buffer_seek(buffer, buffer_seek_start, 12); // skip ".LcfDatabase"
while (quit == false)
{
    var seq_id = buffer_read(buffer, buffer_u8);
    var seq_length = 0;
    
    seq_length = buffer_read_ber(buffer);
    
    //show_debug_message("Database Seq ID: " + string(seq_id) + " size: " + string(seq_length));
    switch (seq_id)
    {
        case 0:
            quit = true;
        break;
    
        // Chipset
        case 20: // 0x14
            ds_map_add_map(database, "chipset", rm2k_read_ldb_chipset(buffer));
            return database;
        break;
        
        default:
            // Skip unsupported/unimplemented sections
            buffer_seek(buffer, buffer_seek_relative, seq_length);
        break;
    
        // Hero
        /*case 11: // 0x0B
        break;
        
        // Special Skill
        case 12: // 0x0C
        break;
        
        // Item
        case 13: // 0x0D
        break;
        
        // Enemies
        case 14: // 0x0E
        break;
        
        // Enemy Group
        case 15: // 0x0F
        break;
        
        // Terrain
        case 16: // 0x10
        break;
        
        // Attribute
        case 17: // 0x11
        break;
        
        // Condition / State 
        case 18: // 0x12
        break;
        
        // Battle Animation / Fighting Animation
        case 19: // 0x13
        break;
        
        // Term
        case 21: // 0x15
        break;
        
        // System
        case 22: // 0x16
        break;
        
        // Switch Names
        case 23: // 0x17
        break;
        
        // Variable Names
        case 24: // 0x18
        break;
        
        // Common Events
        case 25: // 0x19
        break;
        
        // Rm2k3 Events [Not Supported]
        // Common Events - split
        case 26: // 0x1A
        case 27: // 0x1B
        case 28: // 0x1C
        // Combat Command
        case 29: // 0x1D
        // Classes / Occupation
        case 30: // 0x1E
        case 31: // 0x1F
        // Battle Animation 2
        case 32: // 0x20
            show_debug_message("Rpg Maker 2003 database files are not supported.");
        break;*/
    }
}
return database;
