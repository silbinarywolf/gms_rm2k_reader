/*--------------------------------------------------------------------------------------
*   rm2k_map_draw
*
*   Renders the map data provided by rm2k_read_lmu
*
*   argument0 - Map Data (Optional)
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var map_data;
if (argument_count == 1)
{
    map_data = argument[0];
}
else
{
    map_data = _rm2k_map;
}
// Get current view values
var view_x, view_y;
var view_right, view_bottom;
view_x = view_xview[view_current];
view_y = view_yview[view_current];
view_right = view_x + view_wview[view_current];
view_bottom = view_y + view_hview[view_current];

// Draw the part of the map that the current view can see
rm2k_map_draw_tile_region(map_data, view_x, view_y, view_right, view_bottom);
