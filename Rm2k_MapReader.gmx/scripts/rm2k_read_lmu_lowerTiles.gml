/*--------------------------------------------------------------------------------------
*   rm2k_read_lmu_lowerTiles
*
*   Reads the tiles, puts it into a quick array for rendering
*
*   argument0 - Buffer
*   argument1 - Map Data
*   argument2 - Database
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var BLOCK_A = 1000;
var BLOCK_C = 3000;

var BLOCK_D = 4000;
var BLOCK_D_BLOCKS = 12;

var BLOCK_E = 5000;
var BLOCK_E_TILES = 144;

// Requires rm2k_init() be called
var blockA_subtiles = global._rm2k_blockA_subtiles;
var blockD_subtiles = global._rm2k_blockD_subtiles;

var buffer = argument0;
var map_data = argument1;
var database = argument2;

var map_chipset_id = ds_map_find_value(map_data, "id");
var map_width = ds_map_find_value(map_data, "width");
var map_height = ds_map_find_value(map_data, "height");
var map_collision = ds_map_find_value(map_data, "collision");

var database_passable;
if (database != -1)
{
    var database_chipsets = ds_map_find_value(database, "chipset");
    var database_chipset = ds_list_find_value(database_chipsets, map_chipset_id);
    database_passable = ds_map_find_value(database_chipset, "lowerTiles");
}
else
{
    database_passable = -1;
}

var map_tile_info = 0;
var map_tile = -1;
var map_tile_id = -1;
var map_tile_data_size = buffer_read_ber(buffer);

// Init Tile Data Array(s)
var map_tiles = 0;

// Read tiles from LMU
map_tiles[map_height-1,map_width-1] = 0;
for (var yy = 0; yy < map_height; ++yy)
{
    for (var xx = 0; xx < map_width; ++xx)
    {
        // Get the tile ID
        map_tile = buffer_read(buffer, buffer_u16);

        if (map_tile >= BLOCK_E && map_tile < BLOCK_E + BLOCK_E_TILES) {
            map_tile_id = map_tile - BLOCK_E;
            // If Block E
            var row, col;
            // Get the tile coordinates from chipset
            if (map_tile_id < 96) {
                col = 12 + floor(map_tile_id mod 6);
                row = floor(map_tile_id / 6);
            } else {
                // If from second column of the block
                col = 18 + floor((map_tile_id - 96) mod 6);
                row = floor((map_tile_id - 96) / 6);
            }
            map_tile_info[0] = map_tile;
            map_tile_info[1] = col << 4; // fast multiply by 16
            map_tile_info[2] = row << 4;
            map_tiles[yy,xx] = map_tile_info;
            
            // If Database loaded, set collision data
            if (is_array(database_passable))
            {
                ds_grid_set(map_collision,xx,yy,database_passable[map_tile_id + 18]);
            }
        } 
        else if (map_tile >= BLOCK_C && map_tile < BLOCK_D) {
            // If Block C
            map_tile_id = floor((map_tile - BLOCK_C) / 50);
            // Get the tile coordinates from chipset
            var col = floor(3 + map_tile_id);
            var row = 4 + 0; //(0 - 3) //TODO: Implement animation: animation_step_c;
            // Draw the tile
            map_tile_info[0] = map_tile;
            map_tile_info[1] = col << 4; // fast multiply by 16
            map_tile_info[2] = row << 4;
            map_tiles[yy,xx] = map_tile_info;
            
            // If Database loaded, set collision data
            if (is_array(database_passable))
            {
                ds_grid_set(map_collision,xx,yy,database_passable[map_tile_id+3]);
            }
        } 
        else if (map_tile < BLOCK_C) {
            map_tile_id = map_tile;
            // Calculate the A block id
            var block = floor(map_tile / BLOCK_A);
            var animID = 0; // 0 - 2 (Upperbounds)
            
            // Calculate the B block combination
            var block_b_subtile = floor((map_tile - block * BLOCK_A) / 50);
            if (block_b_subtile >= 16) {
                show_message("Invalid AB Autotile ID: block_b_subtile=" + string(block_b_subtile));
                return false;
            }
            
            // Calculate the A block combination
            var block_a_subtile = (map_tile - block * BLOCK_A) - floor(block_b_subtile * 50);
            if (block_a_subtile >= 47) {
                show_message("Invalid AB Autotile ID: block_a_subtile=" + string(block_b_subtile));
                return false;
            }
            
            map_tile_info[0] = map_tile;
            map_tile_info[1] = -1;
            map_tile_info[2] = -1;
            map_tile_info[3] = -1;
            map_tile_info[4] = -1;
            
            // If Database loaded, set collision data
            if (is_array(database_passable))
            {
                ds_grid_set(map_collision,xx,yy,database_passable[block]);
            }
            
            var subtitle_array = blockA_subtiles[block_a_subtile];
            
            var map_tile_subinfo = 0;
            map_tile_subinfo[1] = 0;
        
            // Determine block B subtiles
            var count = 0;
            for (var j = 0; j < 2; j++) {
                for (var i = 0; i < 2; i++) {
                    // Skip the subtile if it will be used one from A block instead
                    if (ds_grid_get(subtitle_array, j, i) != -1)
                    {
                        ++count;
                        continue;
                    }
                    
                    // Get the block B subtiles ids and get their coordinates on the chipset
                    var t = (block_b_subtile >> ((j * 2 + i)) & 1);
                    if (block == 2)
                    {
                        t ^= 3;
                    }
                    
                    var quarter_x = 0; // orig: animID * 16
                    var quarter_y = (4 + t) * 16;

                    map_tile_subinfo[0] = quarter_x + (i << 3); // fast multiply by 8
                    map_tile_subinfo[1] = quarter_y + (j << 3);
                    map_tile_info[1 + count] = map_tile_subinfo;
                    
                    // Reset array
                    map_tile_subinfo = 0;
                    map_tile_subinfo[1] = 0;
                    ++count;
                }
            }
            
            // Determine block A subtiles
            count = 0;
            for (var j = 0; j < 2; j++) {
                for (var i = 0; i < 2; i++) {     
                    // Skip the subtile if it was used one from B block
                    var quarter_y = ds_grid_get(subtitle_array, j, i);
                    if (quarter_y == -1)
                    {
                        ++count;
                        continue;
                    }

                    var quarter_x = 0; // orig: animID
                    if (block == 1)
                    {
                        quarter_x += 3;
                    }

                    map_tile_subinfo[0] = (quarter_x<<4) + (i<<3); // fast multiply by 16 and 8 respectively
                    map_tile_subinfo[1] = (quarter_y<<4) + (j<<3);
                    
                    map_tile_info[1 + count] = map_tile_subinfo;  

                    // Reset array
                    map_tile_subinfo = 0;
                    map_tile_subinfo[1] = 0;
                    ++count;
                }
            }

            // Determine block B subtiles when combining A and B
            if (block_b_subtile != 0 && block_b_subtile != 0)
            {
                count = 0;
                for (var j = 0; j < 2; j++) {
                    for (var i = 0; i < 2; i++) {
                        // Calculate Tile (row 0..3)
                        var t = block_b_subtile >> (((j * 2) + i) & 1);
                        
                        // Skip the subtile if not used
                        if (t == 0)
                        {
                            ++count;
                            continue;
                        }
                        
                        if (block == 2) 
                        {
                            t *= 2;
                        }
                        
                        // Get the coordinates on the chipset
                        var quarter_x = 0; // orig: animID * 16;
                        var quarter_y = (4 + t) << 4; // fast multiply by 16
                    
                        map_tile_subinfo[0] = quarter_x + (i<<3); // fast multiply by 8
                        map_tile_subinfo[1] = quarter_y + (j<<3);
                        
                        map_tile_info[1 + count] = map_tile_subinfo;

                        // Reset array
                        map_tile_subinfo = 0;
                        map_tile_subinfo[1] = 0;
                        ++count;
                    }
                }
            }
            
            map_tiles[yy,xx] = map_tile_info;
        }
        else {
            map_tile_id = map_tile - BLOCK_D;
        
            // Calculate the D block id
            var block = floor(map_tile_id / 50);
            
            // Calculate the D block combination
            var subtile = floor(map_tile_id - block * 50);
            
            // Get Block chipset coords
            var block_x, block_y;
            if (block < 4) {
                // If from first column
                block_x = floor(block % 2) * 3;
                block_y = 8 + floor(block / 2) * 4;
            } 
            else {
                // If from second column
                block_x = floor(6 + (block % 2) * 3);
                block_y = floor(floor((block - 4) / 2) * 4);
            }
            
            map_tile_info[4] = -1;
            map_tile_info[3] = -1;
            map_tile_info[2] = -1;
            map_tile_info[1] = -1;
            map_tile_info[0] = map_tile;
            
            // If Database loaded, get collision data
            if (is_array(database_passable))
            {
                ds_grid_set(map_collision,xx,yy,database_passable[block+6]);
            }
            
            var map_tile_subinfo = 0;
            map_tile_subinfo[1] = 0;
            
            // Draw D block subtiles
            var subtitle_array = blockD_subtiles[subtile];
            var count = 0;
            for (var j = 0; j < 2; j++) {
                for (var i = 0; i < 2; i++) {
                    var quarter_offset = ds_grid_get(subtitle_array, j, i);
                    var quarter_x = (block_x+quarter_offset[0]) << 4; // fast multiply by 16
                    var quarter_y = (block_y+quarter_offset[1]) << 4;

                    map_tile_subinfo[0] = quarter_x + (j<<3); // fast multiply by 8
                    map_tile_subinfo[1] = quarter_y + (i<<3);
                    
                    map_tile_info[1 + count] =  map_tile_subinfo;
                    
                    // Reset array
                    map_tile_subinfo = 0;
                    map_tile_subinfo[1] = 0;
                    
                    ++count;
                }
            }
            
            map_tiles[yy,xx] = map_tile_info;
        }
        map_tile_info = 0;
    }
   // game_end();
   // return false;
}
ds_map_add(map_data, "lowerTiles", map_tiles);
return map_tiles;
