/*--------------------------------------------------------------------------------------
*   rm2k_draw_errors
*
*   Draw the errors that have occurred on the screen.
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
draw_text(view_xview[0]+5,view_yview[0]+5, global._rm2k_errors);
