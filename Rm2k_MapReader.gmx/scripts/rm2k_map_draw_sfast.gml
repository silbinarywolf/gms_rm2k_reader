/*--------------------------------------------------------------------------------------
*   rm2k_map_draw_sfast
*
*   Renders the map data provided by rm2k_read_lmu using surfaces and only updates
*   the tiles on the surface that need updating.
*
*   arguemnt0 - Map Data (Optional)
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
/*var surface, surface_swap, map_data;
surface = _rm2k_surface[view_current];
surface_swap = _rm2k_surface_swap[view_current];
if (argument_count == 1)
{
    map_data = argument[0];
}
else
{
    map_data = _rm2k_map;
}
// Get current view values
var view_x, view_y, view_w, view_h;
var view_right, view_bottom;
view_x = view_xview[view_current];
view_y = view_yview[view_current];
view_w = view_wview[view_current];
view_h = view_hview[view_current];
view_right = view_x + view_w;
view_bottom = view_y + view_h;

// Calculate whats changed in the view and only draw tiles
// that have changed.
var view_x_dist, view_y_dist;
view_x_dist = view_x - _rm2k_surface_left[view_current];
view_y_dist = view_y - _rm2k_surface_top[view_current];

// If enabled surface caching
if (global._rm2k_surface_support)
{
    if (!surface_exists(surface))
    {
        // Calculate surface size based on the current view
        var view_width_pow2, view_height_pow2, surface_dimensions;
        view_width_pow2 = round_to_pow2(view_w);
        view_height_pow2 = round_to_pow2(view_h);
        if (view_width_pow2 > view_height_pow2)
        {
            surface_dimensions = view_width_pow2;
        }
        else
        {
            surface_dimensions = view_height_pow2;
        }
        
        // Render map to surface
        surface = surface_create(surface_dimensions,surface_dimensions);
        if (surface_exists(surface))
        {
            surface_set_target(surface);
            rm2k_map_draw_tile_region(map_data, view_x, view_y, view_x + surface_dimensions, view_y + surface_dimensions, 0, 0);
            surface_reset_target();
            
            _rm2k_surface_left[view_current] = view_x;
            _rm2k_surface_top[view_current] = view_y;
            _rm2k_surface_right[view_current] = view_x + view_w;
            _rm2k_surface_bottom[view_current] = view_y + view_h;
        }
    }
    else
    {
        // If -should- updating the surfaced tile render
        if (view_x_dist >= 16 || view_x_dist <= -16)
        {
            if (!surface_exists(surface_swap))
            {
                surface_swap = surface_create(surface_get_width(surface),surface_get_height(surface));
            }
            // If able to use a swapper
            if (surface_exists(surface))
            {
                if (surface_exists(surface_swap))
                {
                    var view_surface_right = view_x + surface_get_width(surface);
                    var view_surface_bottom = view_y + surface_get_height(surface);
                    while (view_x_dist >= 16)
                    {
                        surface_set_target(surface_swap);
                        surface_copy(surface_swap, 0, 0, surface);
                        //rm2k_map_draw_tile_region(map_data, view_surface_right - 16, view_y, view_surface_right, view_surface_bottom, surface_get_width(surface)-16, 0);
                        surface_reset_target();
                        view_x_dist -= 16;
                        _rm2k_surface_left[view_current] += 16;
                        
                        //show_debug_message("Clocked over. x+16")
                    }
                    while (view_x_dist <= -16)
                    {
                        surface_set_target(surface_swap);
                        surface_copy(surface_swap, 0, 0, surface);
                        rm2k_map_draw_tile_region(map_data, view_x, view_y, view_x+16, view_surface_bottom, 0, 0);
                        surface_reset_target();
                        view_x_dist += 16;
                        _rm2k_surface_left[view_current] -= 16;
                        //show_debug_message("Clocked over. x-16")
                    }
                    // Swap the surfaces
                    var surface_old;
                    surface_old = surface;
                    surface = surface_swap;
                    surface_swap = surface_old;
                    // Update offsets for use in surface render below
                    view_x_dist = view_x - _rm2k_surface_left[view_current];
                    view_y_dist = view_y - _rm2k_surface_top[view_current];
                }
                else
                {
                    show_debug_message("Rm2k - Surface Swap failed");
                }
            }
            else
            {
                show_debug_message("Rm2k - Lost original surface pre-swap");
            }
            show_debug_message("Rm2k - Finished updating surface.");
        }
    }
}
// If no surfaces worked, just draw it normally.
if (!surface_exists(surface) || keyboard_check(ord("Z")))
{
    rm2k_map_draw_tile_region(map_data, view_x, view_y, view_right, view_bottom);
}
else
{
    if (keyboard_check(ord("X")))
    {
        draw_surface(surface_swap, view_x - view_x_dist, view_y - view_y_dist);
        //show_debug_message(string(surface_swap) + " " + string(view_y_dist));
    }
    else
    {
        draw_surface(surface, view_x - view_x_dist, view_y - view_y_dist);
    }
}
_rm2k_surface[view_current] = surface;
_rm2k_surface_swap[view_current] = surface_swap;*/
