/*--------------------------------------------------------------------------------------
*   rm2k_read_lmu_genAB
*
*   Generates an autotile cache and places it in the map.
*
*   argument0 - (ref) AutotileAB Array
*   argument1 - 
*
*   @author SilbinaryWolf, Ghabry (https://github.com/Ghabry)
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
/*var TILES_PER_ROW = 64;

var autotile_ab = argument[0];
var ID = argument[1];
var animID = argument[2];

// Calculate the block to use
//  1: A1 + Upper B (Grass + Coast)
//  2: A2 + Upper B (Snow + Coast)
//  3: A1 + Lower B (Grass + Ocean/Deep water)
var block = ID / 1000;

// Calculate the B block combination
var b_subtile = floor((ID - block * 1000) / 50);
if (b_subtile >= 16) {
    return false;
}

// Calculate the A block combination
var a_subtile = floor(ID - block * 1000 - b_subtile * 50);

var autotile_id = _rm2k_autotiles_ab_next++;
var dst_xy;
dst_xy[1] = floor(autotile_id / TILES_PER_ROW); // Y
dst_xy[0] = floor(autotile_id % TILES_PER_ROW); // X

ds_multigrid_set(autotile_ab, animID, block, b_subtile, a_subtile, dst_xy);
return true;*/
