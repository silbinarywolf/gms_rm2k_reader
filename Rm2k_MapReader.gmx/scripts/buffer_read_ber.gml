/// buffer_read_ber(buffer, type)
/*--------------------------------------------------------------------------------------
*   buffer_read_ber
*
*   Reads a BER encoded integer from the buffer
*
*   argument0 - Buffer
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/liblcf/blob/master/src/reader_lcf.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var buffer, value, temp;
buffer = argument0;
value = 0;
temp = 255;
while (temp & 128)
{
    value = value << 7;
    temp = buffer_read(buffer, buffer_u8);
    //show_debug_message("bufferBer: " + string(temp));
    value |= temp & 127; // 0x7F
}
return value;
