/*--------------------------------------------------------------------------------------
*   rm2k_get_collision
*
*   Checks whether the current object would collide with the world
*
*   argument0 - X Relative
*   argument1 - Y Relative
*   argument2 - Map Data (Optional)
*
*   Returns true if collision was found
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var map_collision;
if (argument_count > 2)
{
    map_collision = ds_map_find_value(argument[2], "collision");
}
else
{
    map_collision = global._rm2k_collision;
}

// Get the collision box offset
// ie. the values you typed into sprites Left, Top
var rel_bbox_x, rel_bbox_y;
rel_bbox_x = bbox_left;
rel_bbox_y = bbox_top;

// Get the real position of where the collision would occur
var xx, yy;
xx = argument[0] + rel_bbox_x;
yy = argument[1] + rel_bbox_y;

// Get object width/height
var this_width, this_height;
this_width = bbox_right - bbox_left;
this_height = bbox_bottom - bbox_top;

// Get grid range to check
var x1, y1, x2, y2;
x1 = xx >> 4; // Divide by 16
y1 = yy >> 4;
x2 = (xx + this_width) >> 4;
y2 = (yy + this_height) >> 4;

// Return true if collision found in range
return (ds_grid_get_sum(map_collision, x1, y1, x2, y2)>0);
