/// rm2k_map_draw_tile_region(map_data, left, top, right, bottom, x, y)
/*--------------------------------------------------------------------------------------
*   rm2k_map_draw_tile_region
*
*   Renders a portion of map data
*
*   argument0 - Map Data
*   argument1 - Left
*   argument2 - Top
*   argument3 - Right
*   argument4 - Bottom
*   argument5 (optional) - Destination X
*   argument6 (optional) - Destination Y
*
*   @author SilbinaryWolf
*   @source https://github.com/EasyRPG/Player/blob/master/src/tilemap_layer.cpp
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var BLOCK_C = 3000;

var BLOCK_D = 4000;
var BLOCK_D_BLOCKS = 12;

var BLOCK_E = 5000;
var BLOCK_E_TILES = 144;

var BLOCK_F = 10000;
var BLOCK_F_TILES = 144;

var map_data = argument[0];

var background_ind = ds_map_find_value(map_data, "background_index");
var map_width = ds_map_find_value(map_data, "width");
var map_height = ds_map_find_value(map_data, "height");
var map_tiles;
if (_rm2k_layer == 0)
{
    map_tiles = ds_map_find_value(map_data, "lowerTiles");
}
else
{
    map_tiles = ds_map_find_value(map_data, "upperTiles");
}

// Setup animation frame variables
// Only necessary for lowerTiles as upperTiles aren't
// animated.
var map_animation_offset_ab, map_animation_offset_c;
if (_rm2k_layer == 0)
{
    if (_rm2k_anim_frame == _rm2k_anim_frame_speed)
    {
        map_animation_offset_ab = 1;
        map_animation_offset_c = 1;
    }
    else if (_rm2k_anim_frame == _rm2k_anim_frame_speed * 2)
    {
        map_animation_offset_ab = 2;
        map_animation_offset_c = 2;
    }
    else if (_rm2k_anim_frame == _rm2k_anim_frame_speed * 3)
    {
        if (_rm2k_anim_type == 1)
        {
            // If animation type is 1-2-3-2
            map_animation_offset_ab = 1;
        }
        else
        {
            // If animation type is 1-2-3
            map_animation_offset_ab = 0;
        }
        map_animation_offset_c = 3;
    }
    else
    {
        map_animation_offset_ab = 0;
        map_animation_offset_c = 0;
    }
    map_animation_offset_ab *= 16;
    map_animation_offset_c *= 16;
}

// Calculate where on the screen to start drawing
// or use the provided destination coords.
var map_draw_x_default = argument[1];
var map_draw_y_default = argument[2];
var map_draw_x, map_draw_y;
if (argument_count >= 6)
{
    map_draw_x_default = argument[5];
    if (argument_count >= 7)
    {
        map_draw_y_default = argument[6];
    }
}
map_draw_x_default = floor(map_draw_x_default / 16) * 16;
map_draw_y_default = floor(map_draw_y_default / 16) * 16;
map_draw_x = map_draw_x_default;
map_draw_y = map_draw_y_default;

// Calculate what region of tiles to draw based on the parameters
var tiles_x_start = floor((argument[1]) / 16);
var tiles_y_start = floor((argument[2]) / 16);
var tiles_x_end = argument[3];
var tiles_y_end = argument[4];
tiles_x_end = ceil(tiles_x_end / 16);
tiles_y_end = ceil(tiles_y_end / 16);
if (tiles_x_start <= 0)
{
    tiles_x_start = 0;
}
if (tiles_y_start <= 0)
{
    tiles_y_start = 0;
}
if (tiles_x_end > map_width)
{
    tiles_x_end = map_width;
}
if (tiles_y_end > map_height)
{
    tiles_y_end = map_height;
}
var map_tile_info, map_tile_subinfo, map_tile_subinfo_count;

for (var yy = tiles_y_start; yy < tiles_y_end; ++yy)
{
    for (var xx = tiles_x_start; xx < tiles_x_end; ++xx)
    {
        map_tile_info = map_tiles[yy, xx];
        if (is_array(map_tile_info))
        {
            if (map_tile_info[0] >= BLOCK_E) // BLOCK E AND F
            {
                draw_background_part(background_ind, map_tile_info[1], map_tile_info[2], 16, 16, map_draw_x, map_draw_y);
            }
            else if (map_tile_info[0] >= BLOCK_C && map_tile_info[0] < BLOCK_D)
            {
                draw_background_part(background_ind, map_tile_info[1], map_tile_info[2] + map_animation_offset_c, 16, 16, map_draw_x, map_draw_y);
            }
            else if (map_tile_info[0] < BLOCK_C) // BLOCK AB
            {
                map_tile_subinfo_count = 0;
                for (j = 0; j < 2; ++j)
                {
                    for (i = 0; i < 2; ++i)
                    {
                        map_tile_subinfo = map_tile_info[1+map_tile_subinfo_count];
                        draw_background_part(background_ind, map_tile_subinfo[0] + map_animation_offset_ab, map_tile_subinfo[1], 8, 8, map_draw_x + (i<<3), map_draw_y + (j<<3));
                        ++map_tile_subinfo_count;
                    }
                }
            }
            else
            {
                map_tile_subinfo_count = 0;
                for (j = 0; j < 2; ++j)
                {
                    for (i = 0; i < 2; ++i)
                    {
                        map_tile_subinfo = map_tile_info[1+map_tile_subinfo_count];
                        draw_background_part(background_ind, map_tile_subinfo[0], map_tile_subinfo[1], 8, 8, map_draw_x + (j<<3), map_draw_y + (i<<3));
                        ++map_tile_subinfo_count;
                    }
                }
            }
        }
        map_draw_x += 16;
    }
    map_draw_x = map_draw_x_default;
    map_draw_y += 16;
}
