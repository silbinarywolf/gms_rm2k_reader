/*--------------------------------------------------------------------------------------
*   rm2k_init_blockA_item
*
*   Creates the second/third row of the 3D array.
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
var item = ds_grid_create(2,2);
ds_grid_set(item, 0, 0, argument0);  
ds_grid_set(item, 0, 1, argument1);
ds_grid_set(item, 1, 0, argument2);  
ds_grid_set(item, 1, 1, argument3);
return item;
