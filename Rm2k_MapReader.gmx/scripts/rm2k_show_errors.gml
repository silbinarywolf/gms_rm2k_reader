/*--------------------------------------------------------------------------------------
*   rm2k_show_errors
*
*   Shows errors that occurred on the screen.
*
*   @author SilbinaryWolf
*   @since 1.0.0
*-------------------------------------------------------------------------------------
*/
show_message(global._rm2k_errors);
